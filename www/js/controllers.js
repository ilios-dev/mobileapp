
var iliosApp;


iliosApp.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/social/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})


iliosApp.controller('LoginCtrl', function ($scope, $state, $window, LogInServ, $ionicPopup, $ionicModal, UserAuthFactory, AuthenticationFactory) {
    'use strict';

    // Set Motion
    //ionicMaterialMotion.ripple();
    //ionicMaterialInk.displayEffect();

    $scope.data = {};

    $scope.logInform = function (data) {
      var username = data.email, password = data.password;

      LogInServ.logInUsr(username, password)
        .success( function (data) {
          AuthenticationFactory.isLogged    = true;
          AuthenticationFactory.user        = data.user.Name ;
          AuthenticationFactory.userRole    = data.user.Group;
          AuthenticationFactory.isInternal  = data.user.Internal;

          AuthenticationFactory.User_id     = data.user.id;
          AuthenticationFactory.UserType_id = data.user.UserTypes_id;
          AuthenticationFactory.Company_id = data.user.Companies_id;

          $window.sessionStorage.token        = data.token;
          $window.sessionStorage.user         = data.user.Name; //  to fetch the user details on refresh
          $window.sessionStorage.userRole     = data.user.Group; // to fetch the user details on refresh
          $window.sessionStorage.User_id      = data.user.id;
          $window.sessionStorage.isInternal   = data.user.Internal;


          $window.sessionStorage.UserType_id = data.user.UserTypes_id;
          $window.sessionStorage.Company_id = data.user.Companies_id;
          $state.go('app.profile');
          //console.log(data);
        })
        .error( function (data) {
          var alertPopUp = $ionicPopup.alert({
            title     : "Log In Failed",
            template  : '<div class="modal-body"> <p> Please use correct details &hellip;</p> </div>  '
          }).then( $scope.data = {} )
        });
    };
});


iliosApp.controller('bookings', function ($scope) {
  'use strict';
  //Models
  angular.extend($scope, {
    date  : new Date,
    anele : "Word !"
  });

  //Methods
  angular.extend($scope, {

  });

});

iliosApp.controller('ScplashCtrl', function ($scope, AuthenticationFactory, $state, UserAuthFactory) {


  // Models
  angular.extend($scope, {
    userCheck : AuthenticationFactory,
  });

  console.log($scope.userCheck);
});

iliosApp.controller('ProfileCtrl', function ($scope, AuthenticationFactory, $state, UserAuthFactory, $controller) {
  'use strict';

  /*var LoginCtrlViewModel = $scope.$new();
  $controller('LoginCtrl',{$scope : LoginCtrlViewModel });
  LoginCtrlViewModel.logInform ();*/

  // Models
  angular.extend($scope, {
    userCheck : AuthenticationFactory,
  });

  angular.extend($scope, {
    profileFunc : function (data) {
      console.log(data);
    },
  });

  //console.log($scope.userCheck);

  if ($scope.userCheck.isLogged === false) {
    UserAuthFactory.logout();
    $state.go('app.start');
  } else {
    console.log($scope.userCheck);
  }
});
