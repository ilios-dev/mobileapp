/* =======================================================
 *
 * =======================================================
 */

var iliosApp;

iliosApp.service('LogInServ',  function ($q, $http, AuthenticationFactory){
	return {
		logInUsr : function (username, password ) {
			var deffered = $q.defer(),
				  promise = deffered.promise;
		      $http.post('http://localhost:3000/login',{
            username : username,
            password : password
		      }).success(function (data) {
              if(data.status !== 401 && data.token !== undefined ){
                 	deffered.resolve(data);
              } else {
                deffered.reject('Something is wrong with you');
            }
          });

			promise.success = function(fn) {
        promise.then(fn);
        return promise;
      };

      promise.error = function(fn) {
        promise.then(null, fn);
        return promise;
      };

			return promise;
		}
	};
});


iliosApp.service('LogOutServ', function ($q, $http){
  return {
    logOutUsr : function (username, password) {
      var deffered = $q.defer(),
          promise = deffered.promise;
      username == '' && password == "";

      promise.success = function(fn) {
        promise.then(fn);
        return promise;
      };

      promise.error = function(fn) {
        promise.then(null, fn);
        return promise;
      };

      return promise;
    }
  }
});
