// Ionic Starter App
var iliosApp, angular;

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js

iliosApp = angular.module('starter', ['ionic', 'ionic-material', 'ionMdInput']);

iliosApp.run(function ($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
});

iliosApp.factory('AuthenticationFactory', function ($window) {
  var auth = {
    isLogged: false,
    userRole : {},
    isInternal : {},
    check: function() {
     // console.log($window.sessionStorage);
      if ($window.sessionStorage.token && $window.sessionStorage.user && $window.sessionStorage.isInternal && $window.sessionStorage.userRole) {
        this.isLogged   = true;
        this.userRole = $window.sessionStorage.userRole;
        this.isInternal = $window.sessionStorage.isInternal;
        this.User_id =  $window.sessionStorage.User_id;
        this.UserType_id = $window.sessionStorage.UserType_id;
        this.Company_id = $window.sessionStorage.Company_id;
      } else {
        this.isLogged = false;
        delete this.user;
      }
    }
  };
  return auth;
});

iliosApp.factory('UserAuthFactory', function($window, $location, $http, AuthenticationFactory, $state) {

  return {
    logout: function() {
      if (AuthenticationFactory.isLogged) {
        AuthenticationFactory.isLogged = false;
        delete AuthenticationFactory.user;
        delete AuthenticationFactory.userRole;

        delete $window.sessionStorage.token;
        delete $window.sessionStorage.user;
        delete $window.sessionStorage.userRole;

        //$location.path("/login");
        //$(".container-fluid, .ng-scope").find("body").removeClass("dashboard-loggedIn");
        //$window.location.reload();
        //alert("Logging out");
        $state.go('app.start');
      }
    }
  };

});
